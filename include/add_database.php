<?php
/**
* Web based SQLite management
* add database form
* check if the database is Ok
* @package SQLiteManager
* @author Frédéric HENNINOT
*/

$tempError = error_reporting();
error_reporting(E_ALL & ~(E_ERROR | E_WARNING | E_PARSE | E_NOTICE | E_CORE_ERROR | E_CORE_WARNING | E_COMPILE_ERROR | E_COMPILE_WARNING | E_USER_ERROR | E_USER_WARNING | E_USER_NOTICE));
$dbFilename = '';
$error = false;
if($action == 'saveDb'){
	if(!empty($_POST['dbname']) && (!empty($_POST['dbpath']) || (count($_FILES['dbRealpath'])))){
		if(isset($_POST['dbpath']))
			$dbFilename = SQLiteStripSlashes($_POST['dbpath']);

		if(is_dir(DEFAULT_DB_PATH) && is_writable(DEFAULT_DB_PATH)){
			if(move_uploaded_file($_FILES['dbRealpath']['tmp_name'], DEFAULT_DB_PATH.$_FILES['dbRealpath']['name']))
				$dbFilename = DEFAULT_DB_PATH.$_FILES['dbRealpath']['name'];
		} else {
			$error = true;
			$message = '<li><span style="color: red; font-size: 11px;">'.Translate::g(144).'</span></li>';
		}

		$ext = substr(strrchr($file_name,'.'),1);
		if(in_array($ext, explode(',', FORBIDDEN_DATABASE_FILE_EXTENSIONS))) {
			$error = true;
			$message = '<li><span style="color: red; font-size: 11px;">'.Translate::g(235).'</span></li>';
		}

		if(DEMO_MODE && $_POST['dbname'] != ':memory:')
			$dbFilename = DEFAULT_DB_PATH.basename(str_replace("\\", '/', $dbFilename));

		$tempDir = dirname($dbFilename);
		if($tempDir == '.') $dbFile = DEFAULT_DB_PATH . $dbFilename;
		else $dbFile = $dbFilename;
		if(!$error) {
			if(isset($_POST['dbVersion']) && $_POST['dbVersion'] && !file_exists($dbFile)) {
				$newDb = SQLiteFactory::sqliteGetInstance($dbFile, $_POST['dbVersion']);
				$newDb->query("CREATE TABLE tempFred (id integer);");
				$newDb->query("DROP TABLE tempFred;");
			} else {
				$newDb = SQLiteFactory::sqliteGetInstance($dbFile);
			}
			if($newDb){
				if($newDb->dbVersion == 2) $newDb->close();
				else $newDb = null;
				$query = 'INSERT INTO database (name, location) VALUES ('.quotes(SQLiteStripSlashes($_POST['dbname'])).', '.quotes($dbFilename).')';
				if(!$db->query($query)) {
					$error = true;
					$message .= '<li><span style="color: red; font-size: 11px;">'.Translate::g(100).'</span></li>';
				} else {
					if(DEBUG) $dbsel = $db->last_insert_id();
					else $dbsel = @$db->last_insert_id();
				}
			}
		} else {
			$error = true;
			$message .= '<li><span style="color: red; font-size: 11px;">'.Translate::g(101).'</span></li>';
		}
	} else {
		$error = true;
		$message .= '<li><span style="color: red; font-size: 11px;">'.Translate::g(102).'</span></li>';
	}
}

error_reporting($tempError);
if(!READ_ONLY && (!WITH_AUTH || (SQLiteFactory::getAuth()->getAccess('properties')))) {
	if(empty($action) || ($action=='passwd') || $error){
		if(!isset($_POST['dbname'])) 	$_POST['dbname'] = '';
		if(!isset($_POST['dbpath'])) 	$_POST['dbpath'] = '';
		if(!isset($_POST['dbFilename'])) $_POST['dbFilename'] = '';
?>
<form name="database" action="main.php" enctype="multipart/form-data" method="post" target="main">
	<table style="width: 400px;">
		<tr>
			<td colspan="2" align="center"><?php echo Translate::g(103); ?></td>
		</tr>
<?php if($error) : ?>
		<tr>
			<td colspan="2" align="center"><?php echo Translate::g(9); ?>&nbsp;:&nbsp;<?php echo ((isset($message))? $message : 'unknown' ); ?></td>
		</tr>
<?php endif; ?>
<?php
		$disabled = false;
		if(isset($_POST['dbVersion'])) $forceDbVersion = $_POST['dbVersion'];
		if(count($sqliteVersionAvailable) == 1) {
			if(!isset($_POST['dbVersion'])) $forceDbVersion = $sqliteVersionAvailable[0];
			if($sqliteVersionAvailable[0] == 2) {
				$disabled = 3;
			} else {
				$disabled = 2;
			}
		} else {
			if(!isset($_POST['dbVersion'])) $forceDbVersion = 3;
		}
?>
		<tr>
			<td align="right"><?php echo Translate::g(19); ?>:&nbsp;</td>
			<td><input type="text" class="text" name="dbname" value="<?php echo $_POST['dbname']; ?>" size="20"></td>
		</tr>
		<tr>
			<td align="right"><?php echo Translate::g(229); ?>:&nbsp;</td>
			<td>
				<label for="dbVersion2">2</label>
				<input type="radio" name="dbVersion" id="dbVersion2" value="2"<?php echo (($forceDbVersion == 2)? ' checked' : '' ).(($disabled == 2)? ' disabled' : '' ); ?> />
				<?php echo str_repeat('&nbsp;', 5)?>
				<label for="dbVersion3">3:</label>
				<input type="radio" name="dbVersion" id="dbVersion3" value="3"<?php echo (($forceDbVersion == 3)? ' checked' : '' ).(($disabled == 3)? ' disabled' : '' ); ?> />
				<span style="font-size: 10px"><?php echo Translate::g(230); ?></span>
			</td>
		</tr>
		<?php if(is_dir(DEFAULT_DB_PATH) && is_writable(DEFAULT_DB_PATH)) : ?>
		<tr>
			<td align="center" colspan="2"><div style="height: 5px; margin: 0 10% 5px; border-bottom: 1px solid black;"></div></td>
		</tr>
		<tr>
			<td></td>
			<td><?php echo Translate::g(143); ?></td>
		</tr>
		<tr>
			<td></td>
			<td><em><?php echo DEFAULT_DB_PATH; ?></em></td>
		</tr>
		<tr>
			<td></td>
			<td><input type="file" class="file" name="dbRealpath" value="<?php echo $_POST['dbpath']; ?>" size="20"></td>
		</tr>
		<?php endif; ?>
		<tr>
			<td align="center" colspan="2"><div style="height: 5px; margin: 0 10% 5px; border-bottom: 1px solid black;"></div></td>
		</tr>
		<tr>
			<td></td>
			<td><?php echo Translate::g(234); ?></td>
		</tr>
		<tr>
			<td></td>
			<td><input type="text" class="text" name="dbpath" value="<?php echo $dbFilename; ?>" size="40"></td>
		</tr>
		<tr>
			<td colspan="2" align="center">
				<input class="button" type="submit" value="<?php echo Translate::g(51); ?>" />
			</td>
		</tr>
	</table>
	<input type="hidden" name="action" value="saveDb" />
</form>
<?php
	} else {
		if(!$noDb) echo "<script type=\"text/javascript\">parent.main.location='main.php?dbsel=$dbsel'; parent.left.location='left.php?dbsel=$dbsel';</script>";
		else echo "<script type=\"text/javascript\">parent.location='index.php?dbsel=$dbsel';</script>";
	}
}
?>
