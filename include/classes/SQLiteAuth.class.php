<?php
/**
* Web based SQLite management
* Class for manage user authentification
* @package SQLiteManager
* @author Frédéric HENNINOT
* @version $Id
*/

class SQLiteAuth {

	/**
	* user identification
	*
	* @access private
	* @var integer
	*/
	var $user;

	/**
	* user information
	*
	*/
	var $userInformation;

	/**
	* Class constructor
	*
	* @access public
	*/
	function __construct(){
		if($GLOBALS['action'] == 'logout') {
			$_SESSION['SQLiteManagerConnected'] = false;
			unset($_SESSION['SQLiteManagerUserId']);
			$_SESSION['oldUser'] = $_SERVER['PHP_AUTH_USER'];
			session_write_close();
			echo "<script type=\"text/javascript\">parent.location='index.php';</script>";
			exit;
		}
		if(!isset($_SESSION['SQLiteManagerConnected']) || !$_SESSION['SQLiteManagerConnected']){
			if((isset($_SESSION['oldUser']) && ($_SESSION['oldUser'] == $_SERVER['PHP_AUTH_USER'])) || !isset($_SERVER['PHP_AUTH_USER'])) {
				unset($_SESSION['oldUser']);
				$this->authenticate();
			} else {
				$this->userInformation = $this->getAuthParam();
				$this->user = $_SESSION['SQLiteManagerUserId'] = $this->userInformation['user_id'];
				$_SESSION['SQLiteManagerConnected'] = true;
			}
		} else {
			$this->userInformation = $this->getAuthParam();
			$this->user = $_SESSION['SQLiteManagerUserId'] = $this->userInformation['user_id'];
		}
	}


	/**
	* get user connected information
	*
	* @access public
	*/
	function getAuthParam(){
		if(isset($_SERVER['PHP_AUTH_USER'])) $login = $_SERVER['PHP_AUTH_USER'];
		else $login = '';
		if(isset($_SERVER['PHP_AUTH_PW'])) $passwd = $_SERVER['PHP_AUTH_PW'];
		else $passwd = '';
		$query = '	SELECT user_id, user_name, user_passwd, del, empty, export, data, execSQL, properties, groupe_name, groupe_id
					FROM users , groupes
					WHERE user_groupe_id = groupe_id
						AND user_login='.quotes($login);
		$infoUser = $GLOBALS["db"]->array_query($query);
		if(empty($infoUser)) {
			$_SESSION['SQLiteManagerConnected'] = false;
			unset($_SESSION['SQLiteManagerUserId']);
			$_SESSION['oldUser'] = $_SERVER['PHP_AUTH_USER'];
			displayError(Translate::g(148));
			exit;
		} else {
			$passwdOk = false;
			if(count($infoUser)>1) {
				foreach($infoUser as $infoNum=>$infoOneUser){
					if($infoOneUser['user_passwd'] == md5($passwd)){
						$numUser = $infoNum;
						$passwdOk = true;
					}
				}
			} elseif($infoUser[0]['user_passwd'] == md5($passwd)) $passwdOk = true;
			if(!$passwdOk) {
				$_SESSION['oldUser'] = $_SERVER['PHP_AUTH_USER'];
				displayError(Translate::g(149));
				exit;
			}
		}
		if(!isset($numUser)) $numUser = 0;
		return $infoUser[$numUser];
	}

	/**
	* Send HTTP authentification FORM
	*
	* @access public
	*/
	function authenticate(){
		header('WWW-Authenticate: Basic realm="SQLiteManager"');
    	header('HTTP/1.0 401 Unauthorized');
		displayError(Translate::g(147));
		exit;
	}

	/**
	* get groupe_id
	*
	* @access public
	*/
	function getGroupeId(){
		if(is_array($this->userInformation) && !empty($this->userInformation))
			return $this->userInformation['groupe_id'];
	}

	/**
	* return true if 'Admin'
	*
	* @access public
	*/
	function isAdmin(){
		if(is_array($this->userInformation) && !empty($this->userInformation)) {
			if($this->userInformation['groupe_id']==1) return true;
			else return false;
		}
	}

	/**
	* Return acces controle for module
	*
	* @access public
	* @param string $module module name
	*/
	function getAccess($module){
		if(is_array($this->userInformation) && !empty($this->userInformation))
			if(isset($this->userInformation[$module]))
			    return $this->userInformation[$module];
			else
			    return false;
	}
}
?>