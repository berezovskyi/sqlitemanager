<?php
/**
* Web based SQLite management
* Class for manage user authentification
* @package SQLiteManager
* @author Frédéric HENNINOT
* @version $Id
*/

class SQLiteAuthProperties extends SQLiteAuth {

	/**
	* Manage Groupe and user
	*
	* @access public
	*/
	function manageAuth(){
		if(!isset($GLOBALS['auth_action'])) $GLOBALS['auth_action'] = '';
		echo '<h2><a href="main.php" class="home">'.Translate::g(190).'</a></h2>';
		switch($GLOBALS['auth_action']){
			case '':
			default:
			case 'passwdUser':
				$this->viewPrivileges();
				break;
			case 'modifyUser':
			case 'addUser':
				$this->viewPrivileges(true);
				break;
			case 'deleteUser':
				if($_REQUEST['user']!=1) $GLOBALS['db']->query('DELETE FROM users WHERE user_id='.$_REQUEST['user']);
				$this->viewPrivileges();
				break;
			case 'savePasswd':
				break;
			case 'modifyGroupe':
			case 'addGroupe':
				$this->viewPrivileges(false, true);
				break;
			case 'deleteGroupe':
				if($_REQUEST['groupe']!=1) $GLOBALS['db']->query('DELETE FROM groupes WHERE groupe_id='.$_REQUEST['groupe']);
				$this->viewPrivileges();
				break;
			case 'saveUser';
				if(!empty($_POST['name']) && !empty($_POST['login']) && !empty($_POST['groupe_id'])){
					if(isset($_REQUEST['user']) && !empty($_REQUEST['user'])){
						$query = 'UPDATE users SET user_groupe_id='.$_POST['groupe_id'].', user_name='.quotes($_POST['name']).', user_login='.quotes($_POST['login']).' WHERE user_id='.$_POST['user'];
					} else {
						$query = 'INSERT INTO users (user_name, user_login, user_groupe_id, user_passwd) VALUES ('.quotes($_POST['name']).', '.quotes($_POST['login']).', '.$_POST["groupe_id"].', '.quotes(md5('')).');';
					}
					if(!empty($query)) $GLOBALS['db']->query($query);
				}
				$this->viewPrivileges();
				break;
			case 'saveGroupe':
				if(!empty($_POST['groupe_name'])){
					if(isset($_REQUEST['groupe']) && !empty($_REQUEST['groupe'])){
						$query = '	UPDATE groupes ' .
								'	SET 	groupe_name='.quotes($_POST['groupe_name']).',' .
								' 			properties='.$_POST['properties'].', ' .
								'			execSQL='.$_POST['execSQL'].', ' .
								'			data='.$_POST['data'].', ' .
								'			export='.$_POST['export'].', ' .
								'			empty='.$_POST['empty'].', ' .
								'			del='.$_POST['del'].
								' 	WHERE groupe_id='.$_REQUEST['groupe'];
					} else {
						$query = 'INSERT INTO groupes (groupe_name, properties, execSQL, data, export, empty, del) '.
                     'VALUES ('.quotes($_POST['groupe_name']).', '.quotes($_POST['properties']).', '.quotes($_POST['execSQL']).', '.quotes($_POST['data']).', '.quotes($_POST['export']).', '.quotes($_POST['empty']).', '.quotes($_POST['del']).')';
					}
					if(!empty($query)) {
						$GLOBALS['db']->query($query);
					}
				}
				$this->viewPrivileges();
				break;
		}
	}

	/**
	* View all privileges information
	*
	* @access public
	*/
	function viewPrivileges($withFormUser=false, $withFormGroupe=false){
		$db = SQLiteFactory::sqliteGetInstance(SQLiteDb);
		$query = '	SELECT user_id, user_name AS '.quotes(Translate::g(163)).',
						user_login AS '.quotes(Translate::g(164)).',
						groupe_name AS '.quotes(Translate::g(165)).'
					FROM users, groupes WHERE user_groupe_id=groupe_id;';
		$tabUser = new SQLiteToGrid($db, $query, 'PrivUser', true, 10, '95%');
		$tabUser->enableSortStyle(false);
		$tabUser->hideColumn(0);
		$tabUser->setGetVars('?action=auth');
		if($tabUser->getNbRecord()<=10) $tabUser->disableNavBarre();
		$tabUser->addCalcColumn(Translate::g(33), '	<a href="?action=auth&amp;auth_action=modifyUser&amp;user=#%0%#" class="Browse">'.displayPics('edit.png', Translate::g(14)).'</a>&nbsp;
											<a href="?action=auth&amp;auth_action=deleteUser&amp;user=#%0%#" class="Browse">'.displayPics('edittrash.png', Translate::g(15)).'</a>&nbsp;
											<a href="?action=auth&amp;auth_action=passwdUser&amp;user=#%0%#" class="Browse">'.displayPics('encrypted.png', Translate::g(157)).'</a>&nbsp;', 'center', 999);
		$tabUser->addCaption('bottom', '<a href="?action=auth&amp;auth_action=addUser" class="Browse">'.Translate::g(159).'</a>');
		$tabUser->disableOnClick();
		$tabUser->build();

		// ------------------------------------------------------------------------
		$query = 'SELECT groupe_id, groupe_name AS '.quotes(Translate::g(163)).',
						CASE properties WHEN 1 THEN '.quotes(Translate::g(191)).' ELSE '.quotes(Translate::g(192)).' END AS '.quotes(Translate::g(61)).',
						CASE execSQL WHEN 1 THEN '.quotes(Translate::g(191)).' ELSE '.quotes(Translate::g(192)).' END AS '.quotes(Translate::g(166)).',
						CASE data WHEN 1 THEN '.quotes(Translate::g(191)).' ELSE '.quotes(Translate::g(192)).' END AS '.quotes(Translate::g(167)).',
						CASE export WHEN 1 THEN '.quotes(Translate::g(191)).' ELSE '.quotes(Translate::g(192)).' END AS '.quotes(Translate::g(168)).',
						CASE empty WHEN 1 THEN '.quotes(Translate::g(191)).' ELSE '.quotes(Translate::g(192)).' END AS '.quotes(Translate::g(169)).',
						CASE del WHEN 1 THEN '.quotes(Translate::g(191)).' ELSE '.quotes(Translate::g(192)).' END AS '.quotes(Translate::g(170)).'
					FROM groupes;';
		$tabGroupe = new SQLiteToGrid($db, $query, 'PrivGroupe', true, 10, '95%');
		$tabGroupe->enableSortStyle(false);
		$tabGroupe->hideColumn(0);
		$tabGroupe->setGetVars('?action=auth');
		if($tabGroupe->getNbRecord()<=10) $tabGroupe->disableNavBarre();
		$tabGroupe->addCalcColumn(Translate::g(33), '	<a href="?action=auth&amp;auth_action=modifyGroupe&amp;groupe=#%0%#" class="Browse">'.displayPics('edit.png', Translate::g(14)).'</a>&nbsp;
											<a href="?action=auth&amp;auth_action=deleteGroupe&amp;groupe=#%0%#" class="Browse">'.displayPics('edittrash.png', Translate::g(15)).'</a>&nbsp;', 'center', 999);
		$tabGroupe->addCaption('bottom', '<a href="?action=auth&amp;auth_action=addGroupe" class="Browse">'.Translate::g(160).'</a>');
		$tabGroupe->disableOnClick();
		$tabGroupe->build();
?>
		<table align="center" class="Browse">
			<tr>
				<td align="center" valign="top">
					<div class="Rights">
						<div style="text-align: center;"><?php echo Translate::g(161); ?></div>
						<?php echo $tabUser->show(); ?>
<?php if($withFormUser) :
		$selectedUser = '';
		$dataGroupe = array();
		if(isset($_REQUEST["user"])) {
			$dataUser = $this->getUserInfo(Request::getInt('user'));
			if(!empty($dataUser))
					$selectedUser = $dataUser["user_groupe_id"];

			$groupeList = $db->array_query("SELECT groupe_id, groupe_name FROM groupes");
			foreach($groupeList as $groupe)
				$dataGroupe[$groupe["groupe_id"]] = $groupe["groupe_name"];
		}

		if(isset($_REQUEST["groupe"])) {
			$groupeName = '';
			$dataGroupe = $this->getGroupeInfo(Request::getInt('groupe'));
			if(isset($dataGroupe["groupe_name"]))
				$groupeName = $dataGroupe["groupe_name"];
			if(!isset($dataGroupe["properties"]))
				$dataGroupe["properties"] = $dataGroupe["execSQL"] = $dataGroupe["data"] = $dataGroupe["export"] = $dataGroupe["empty"] = $dataGroupe["del"] = 0;
		}
?>
						<hr style="border: 1px dashed black; width: 90%;">
						<form name="user" method="post" action="main.php" target="main">
							<table style="font-size: 10px">
								<tr>
									<td><?php echo Translate::g(163); ?></td>
									<td><input type="text" class="text" name="name" value="<?php echo ((!empty($dataUser))? $dataUser["user_name"] : "" ); ?>"></td>
								</tr>
								<tr>
								<td><?php echo Translate::g(164); ?></td>
								<td><input type="text" class="text" name="login" value="<?php echo ((!empty($dataUser))? $dataUser["user_login"] : "" ); ?>"></td>
							</tr>
							<tr>
								<td><?php echo Translate::g(165); ?></td>
								<td>
									<select name="groupe_id">
										<option value="" />
					<?php
						if(count($dataGroupe))
							foreach($dataGroupe as $id=>$value) {
					?>
										<option value="<?php echo $id; ?>"<?php echo (($id==$selectedUser)? ' selected="selected"' : '' ); ?>><?php echo $value; ?></option>
					<?php
							}
					?>
									</select>
								</td>
							</tr>
							<tr>
								<td colspan="2" align="center"><input class="button" type="submit" value="<?php echo Translate::g(51); ?>"></td>
							</tr>
						</table>
						<input type="hidden" name="action" value="<?php echo $GLOBALS["action"]; ?>">
						<input type="hidden" name="user" value="<?php echo ((isset($GLOBALS["user"]))? $GLOBALS["user"] : "" ); ?>">
						<input type="hidden" name="auth_action" value="saveUser">
					</form>
<?php endif; ?>
<?php if(Request::getWord('auth_action') == 'passwdUser') : ?>
						<hr style="border: 1px dashed black; width: 90%;">
						<?php echo $this->changePasswd(); ?>
<?php endif; ?>
					</div>
				</td>
				<td align="center" valign="top">
					<div class="Rights">
						<div align="center"><?php echo Translate::g(162); ?></div>
						<?php echo $tabGroupe->show(); ?>
<?php if($withFormGroupe) : ?>
						<hr style="border: 1px dashed black; width: 90%;">
						<form name="groupe" method="post" action="main.php" target="main">
							<table style='font-size: 10px'>
								<tr>
									<td><?php echo Translate::g(163); ?></td>
									<td><input type="text" class="text" name="groupe_name" value="<?php echo $groupeName; ?>"></td>
								</tr>
								<tr>
									<td><?php echo Translate::g(61); ?></td>
									<td>
										<input type="radio" name="properties" id="properties1" value="1"<?php if($dataGroupe["properties"]) : ?> checked="checked"<?php endif; ?>>
										<label for="properties1"><?php echo Translate::g(40); ?></label>
										<input type="radio" name="properties" id="properties0" value="0"<?php if(!$dataGroupe["properties"]) : ?> checked="checked"<?php endif; ?>>
										<label for="properties0"><?php echo Translate::g(41); ?></label>
									</td>
								</tr>
								<tr>
									<td><?php echo Translate::g(166); ?></td>
									<td>
										<input type="radio" name="execSQL" id="execSQL1" value="1"<?php if($dataGroupe["execSQL"]) : ?> checked="checked"<?php endif; ?>>
										<label for="execSQL1"><?php echo Translate::g(40); ?></label>
										<input type="radio" name="execSQL" id="execSQL0" value="0"<?php if(!$dataGroupe["execSQL"]) : ?> checked="checked"<?php endif; ?>>
										<label for="execSQL0"><?php echo Translate::g(41); ?></label>
									</td>
								</tr>
								<tr>
									<td><?php echo Translate::g(167); ?></td>
									<td>
										<input type="radio" name="data" id="data1" value="1"<?php if($dataGroupe["data"]) : ?> checked="checked"<?php endif; ?>>
										<label for="data1"><?php echo Translate::g(40); ?></label>
										<input type="radio" name="data" id="data0" value="0"<?php if(!$dataGroupe["data"]) : ?> checked="checked"<?php endif; ?>>
										<label for="data0"><?php echo Translate::g(41); ?></label>
									</td>
								</tr>
								<tr>
									<td><?php echo Translate::g(168); ?></td>
									<td>
										<input type="radio" name="export" id="export1" value="1"<?php if($dataGroupe["export"]) : ?> checked="checked"<?php endif; ?>>
										<label for="export1"><?php echo Translate::g(40); ?></label>
										<input type="radio" name="export" id="export0" value="0"<?php if(!$dataGroupe["export"]) : ?> checked="checked"<?php endif; ?>>
										<label for="export0"><?php echo Translate::g(41); ?></label>
									</td>
								</tr>
								<tr>
									<td><?php echo Translate::g(169); ?></td>
									<td>
										<input type="radio" name="empty" id="empty1" value="1"<?php if($dataGroupe["empty"]) : ?> checked="checked"<?php endif; ?>>
										<label for="empty1"><?php echo Translate::g(40); ?></label>
										<input type="radio" name="empty" id="empty0" value="0"<?php if(!$dataGroupe["empty"]) : ?> checked="checked"<?php endif; ?>>
										<label for="empty0"><?php echo Translate::g(41); ?></label>
									</td>
								</tr>
								<tr>
									<td><?php echo Translate::g(170); ?></td>
									<td>
										<input type="radio" name="del" id="del1" value="1"<?php if($dataGroupe["del"]) : ?> checked="checked"<?php endif; ?>>
										<label for="del1"><?php echo Translate::g(40); ?></label>
										<input type="radio" name="del" id="del0" value="0"<?php if(!$dataGroupe["del"]) : ?> checked="checked"<?php endif; ?>>
										<label for="del0"><?php echo Translate::g(41); ?></label>
									</td>
								</tr>
								<tr>
									<td colspan="2" align="center">
										<input class="button" type="submit" value="<?php echo Translate::g(51); ?>">
									</td>
								</tr>
							</table>
							<input type="hidden" name="action" value="auth">
							<input type="hidden" name="groupe_id" value="<?php if(isset($_REQUEST["groupe"])) echo Request::getInt('groupe'); ?>">
							<input type='hidden' name="auth_action" value="saveGroupe">
						</form>
<?php endif; ?>
					</div>
				</td>
			</tr>
		</table>
<?php
	}

	/**
	* Get user's information
	*
	* @access public
	* @param int $user user ID
	* @return array
	*/
	function getUserInfo($user){
		if(isset($_POST) && isset($_POST["user"])){
			$out[0]["user_name"] 		= $_POST["user_name"];
			$out[0]["user_login"] 		= $_POST["user_login"];
			$out[0]["user_groupe_id"] 	= $_POST["user_groupe_id"];
			return $out;
		} else {
			$query = "SELECT user_name, user_login, user_groupe_id FROM users WHERE user_id=".$user;
			$out = $GLOBALS["db"]->array_query($query);
			return $out[0];
		}
	}

	/**
	* Get groupe's information
	*
	* @access public
	* @param int $group groupe_id
	* @return array
	*/
	function getGroupeInfo($group){
		$query = "SELECT * FROM groupes WHERE groupe_id=".$group;
		$out = $GLOBALS["db"]->array_query($query);
		return $out[0];
	}

	/**
	* change password form
	*/
	function changePasswd(){
		$error = false;
		$err_message = "";
		$action = Request::getWord('passwd_action', '', 'post');
		$user = Request::getInt('user', $_SESSION["SQLiteManagerUserId"]);
		if($action == 'save'){
			$user = Request::getInt('user', 0, 'post');
			$db = SQLiteFactory::sqliteGetInstance(SQLiteDb);
			$query = 'SELECT user_passwd FROM users WHERE user_id='.$user;
			$db->query($query);
			$passCurrent = $db->fetch_single();
			$oldPass = Request::getString('old', '', 'post');
			$pass = Request::getString('pass', '', 'post');
			$confirm = Request::getString('confirm', '', 'post');
			if($passCurrent != md5($oldPass)){
				$error = true;
				$err_message = Translate::g(171);
			} else if($pass != $confirm){
				$error = true;
				$err_message = Translate::g(172);
			}

			if(!$error){
				$query = 'UPDATE users SET user_passwd=' . quotes(md5($pass)).' WHERE user_id='.$user;
				$res = $db->query($query);
				if($res) {
			?>
				<table class="tabProp passwd">
					<tr>
						<td colspan="2" align="center"><?php echo Translate::g(157); ?></td>
					</tr>
					<tr>
						<td colspan="2" align="center">
							<div class="info">
							<?php echo Translate::g(173); ?>
							<?php if($user == $_SESSION["SQLiteManagerUserId"]) : ?>
							<br>
							<a href="index.php?action=logout" target="_parent" class='Browse'><?php echo Translate::g(174); ?></a>
							<?php endif; ?>
							</div>
						</td>
					</tr>
				</table>
			<?php
				} else {
					$error = true;
					$err_message = Translate::g(236);
				}
			}
		}

		if($error || !$action) {
		?>
			<form name="passwd" method="post" action="main.php" target="main">
				<table class="tabProp passwd">
					<tr>
						<td colspan="2" align="center"><?php echo Translate::g(157); ?></td>
					</tr>
					<?php if($error) : ?>
					<tr>
						<td colspan="2" align="center">
							<div class="error"><?php echo $err_message; ?></div>
						</td>
					</tr>
					<?php endif; ?>
					<?php if(!$error && isset($message) && !empty($message)) : ?>
					<tr>
						<td colspan="2" align="center">
							<div class="info">
							<?php echo $message; ?>
							<?php if(!isset($_REQUEST["auth_action"])) : ?>
							<br>
							<a href="index.php?action=logout" target="_parent" class='Browse'><?php echo Translate::g(174); ?></a>
							<?php endif; ?>
							</div>
						</td>
					</tr>
					<?php endif; ?>
					<tr>
						<td align="right" style="white-space: nowrap;"><?php echo Translate::g(175); ?></td>
						<td><input type="password" class="text" name="old" size="10"></td>
					</tr>
					<tr>
						<td align="right" style="white-space: nowrap;"><?php echo Translate::g(176); ?></td>
						<td><input type="password" class="text" name="pass" size="10"></td>
					</tr>
					<tr>
						<td align="right" style="white-space: nowrap;"><?php echo Translate::g(177); ?></td>
						<td><input type="password" class="text" name="confirm" size="10"></td>
					</tr>
					<tr>
						<td colspan="2" align="center"><input class="button" type="submit" value="<?php echo Translate::g(51); ?>"></td>
					</tr>
				</table>
				<input type="hidden" name="action" value="auth">
				<input type="hidden" name="user" value="<?php echo $user; ?>">
				<input type="hidden" name="passwd_action" value="save">
				<input type="hidden" name="auth_action" value="passwdUser">
			</form>
		<?php
		}
	}
}
?>
