<?php
class SQLiteFactory {

	public static $dbItems = array('Table', 'View', 'Trigger', 'Function');

    public static function sqliteGetInstance($dbPath, $forceVersion = null, $forceReconnect = false) {
    	static $dbInstance = array();

    	$key = md5($dbPath);

    	if(!isset($dbInstance[$key]) || $forceReconnect) {
	    	if(!$forceVersion) {
	    		if($dbPath == ':memory:')
	    			$dbVersion = min($GLOBALS['sqliteVersionAvailable']);
	    		else
	    			$dbVersion = sqlite::getDbVersion($dbPath);
	    	} else {
	    		$dbVersion = $forceVersion;
	    	}

			if($dbVersion && (($dbVersion == 2) || ($dbVersion == 3)) ) {
		    	$classObj = 'sqlite_' . $dbVersion;
				$dbInstance[$key] = new $classObj($dbPath);
	    	}
    	}

    	if(isset($dbInstance[$key]))
	    	return $dbInstance[$key];

    	return false;
    }

    public static function getAuth() {
    	static $authInstance = null;

    	if(is_null($authInstance))
    		$authInstance = new SQLiteAuth();

    	return $authInstance;
    }

    public static function getTheme() {
    	static $currentTheme = null;

    	if(is_null($currentTheme)) {
    		$request = Request::getInstance();
    		if($request->exists('Theme')) {
    			$currentTheme = Request::getWord('Theme', false);
    			setcookie('SQLiteManager_currentTheme',$currentTheme,1719241200,'/');
    			$_COOKIE['SQLiteManager_currentTheme'] = $currentTheme;
    			echo "<script type=\"text/javascript\">parent.location='index.php';</script>";
    		} elseif(isset($_COOKIE['SQLiteManager_currentTheme'])) {
    			$currentTheme = Request::getWord('SQLiteManager_currentTheme', 0, 'cookie');
    		} else {
    			$currentTheme = 'green';
    		}

    		if(!file_exists(BASE_DIR . "/theme/".$currentTheme."/define.php")) {
    			unset($_COOKIE["SQLiteManager_currentTheme"]);
    			$currentTheme = "default";
    		}
    	}

    	return $currentTheme;
    }

    public static function isHTML() {
    	static $allHTML = null;

    	if(is_null($allHTML)) {
    		$request = Request::getInstance();
    		if($request->exists('HTMLon')) {
    			$allHTML = Request::getInt('HTMLon', 0);
    			setcookie('SQLiteManager_HTMLon',$allHTML,1719241200,'/');
    			$_COOKIE['SQLiteManager_HTMLon'] = $allHTML;
    		} elseif(isset($_COOKIE['SQLiteManager_HTMLon'])) {
    			$allHTML = Request::getInt('SQLiteManager_HTMLon', 0, 'cookie');
    		} else {
    			$allHTML = true;
    		}
    	}

    	 return $allHTML;
    }

    public static function isFullText() {
    	static $allFullText = null;

    	if(is_null($allFullText)) {
    		$request = Request::getInstance('get');
    		if($request->exists('fullText')) {
	    		$allFullText = Request::getInt('fullText', 0);
    			setcookie('SQLiteManager_fullText',$allFullText,1719241200,'/');
    			$_COOKIE['SQLiteManager_fullText'] = $allFullText;
    		} elseif(isset($_COOKIE['SQLiteManager_fullText'])) {
    			$allFullText = Request::getInt('SQLiteManager_fullText', 0, 'cookie');
    		} else {
    			$allFullText = true;
    		}
    	}

    	return $allFullText;
    }
}
?>