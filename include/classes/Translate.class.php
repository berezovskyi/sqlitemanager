<?php
/**
* Web based SQLite management
* Multilingual management
* @package SQLiteManager
* @author Frédéric HENNINOT
* @version $Id: SQLite.i18n.php,v 1.25 2006/04/14 15:16:52 freddy78 Exp $ $Revision: 1.25 $
*/

class Translate
{

	/**
	* Array of all text
	* @var array
	* @access private
	*/
	protected $tabText;

	/**
	* Character encoding
	* @var string
	* @access private
	*/
	public $encoding;

	public $currentLangue = '';

	public $langSuffix='';

	public $itemTranslated;

	public $langueTranslated;

	public $themeTranslated;

	public static $availableLangue = array(	1=>'french', 2=>'english', 3=>'polish',
											4=>'german', 5=>'japanese', 6=>'italian',
											7=>'croatian', 8=>'brazilian_portuguese', 9=>'dutch',
											10=>'spanish', 11=>'danish', 12=>'traditional_chinese',
											13=>'simplified_chinese');

	protected static $languageCode = array('fr'=>1, 'en'=>2, 'sz'=>13, 'es'=>10, 'zh'=>12, 'pl'=>3, 'ja'=>5, 'it'=>6, 'de'=>4, 'da'=>11, 'hr'=>7, 'pt'=>8);

	/**
	* Class constructor
	*
	* @access public
	* @param $tableau array
	*/
	public function __construct($current, $tableau){
		$this->currentLangue = $current;

		if(is_array($tableau))
			$this->tabText = $tableau;

	}

	public function init($encoding, $suffix, $item, $langues, $themes) {
		$this->encoding = $encoding;
		$this->langSuffix = $suffix;
		$this->itemTranslated = $item;
		$this->langueTranslated = $langues;
		$this->themeTranslated = $themes;
	}

	public static function getInstance() {
		static $instance = null;

		if(is_null($instance)) {
			if(isset($_POST['Langue'])) {
				$currentLangue = $_POST['Langue'];
				setcookie('SQLiteManager_currentLangue',$_POST['Langue'],1719241200,'/');
				$_COOKIE['SQLiteManager_currentLangue'] = $currentLangue = $_POST['Langue'];
				echo "<script type=\"text/javascript\">parent.location='index.php';</script>";
			} elseif(isset($_COOKIE['SQLiteManager_currentLangue'])) {
				$currentLangue = $_COOKIE['SQLiteManager_currentLangue'];
			} else {
				$lang = '';
				if(isset($_SERVER['HTTP_ACCEPT_LANGUAGE'])) {
					$lang=strtolower(substr($_SERVER['HTTP_ACCEPT_LANGUAGE'],0,2));
				}
				if(isset(self::$languageCode[$lang]))
					$currentLangue = self::$languageCode[$lang];
				else
					$currentLangue = 2;
			}

			if(isset(self::$availableLangue[$currentLangue]) && file_exists('./lang/'.self::$availableLangue[$currentLangue].'.inc.php')){
				require_once './lang/'.self::$availableLangue[$currentLangue].'.inc.php';
			} else {
				require_once './lang/english.inc.php';
			}

			$instance = new Translate($currentLangue, $TEXT);
			$instance->init($charset, $langSuffix, $itemTranslated, $langueTranslated, $themeTranslated);
		}

		return $instance;
	}
	/**
	* Get the good text to display
	*
	* @access public
	* @param $index int numero du message
	* @param $default string message par défaut
	*/
	public function get($index,$default='No translate'){
		if($index){
			if(isset($this->tabText[$index]) && $this->tabText[$index]){
			  $res = @htmlentities($this->tabText[$index],ENT_NOQUOTES,$this->encoding);
			  $res = str_replace('&lt;','<',$res);
			  $res = str_replace('&gt;','>',$res);
			  $res = str_replace('&amp;','&',$res);
				return $res;
			} else {
				return $default . ' ' . $index;
			}
		}
	}

	public static function __callStatic($name, $arguments) {
		return self::callStatic($name, $arguments);
	}

	public function __call($name, $arguments) {
		return self::callStatic($name, $arguments);
	}

	protected static function callStatic($name, $arguments) {
		$default = 'No translate';
		if (isset($arguments[1]))
			$default = $arguments[1];

		switch($name) {
			case 'g':
				return Translate::getInstance()->get($arguments[0], $default);
				break;
			case 'encoding':
				return Translate::getInstance()->encoding;
				break;
			case 'l':
				return Translate::getInstance()->langSuffix;
				break;
		}
	}
}
?>