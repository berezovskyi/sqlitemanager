<?php
/**
* Web based SQLite management
* Some defines
* @package SQLiteManager
* @author Frédéric HENNINOT
* @version $Id: defined.inc.php,v 1.89 2006/04/17 18:58:20 freddy78 Exp $ $Revision: 1.89 $
*/
include_once "./include/user_defined.inc.php";

define('SQLiteManagerVersion', '1.2.4');

define('BASE_DIR', dirname(__DIR__));

if(!defined('INCLUDE_LIB')) define ('INCLUDE_LIB',dirname(__FILE__).'/');


if (!defined('DEBUG'))
	define('DEBUG',(strpos(SQLiteManagerVersion,'CVS')!==false));

// Default Folder for Uploaded file Database
if(!defined('DEFAULT_DB_PATH')) {
  define('DEFAULT_DB_PATH', BASE_DIR . '/');
}

if(!defined('FORBIDDEN_DATABASE_FILE_EXTENSIONS')) define('FORBIDDEN_DATABASE_FILE_EXTENSIONS', '.php,.exe,.dll');

if(!defined('WITH_AUTH')) define('WITH_AUTH', true);

if(!defined('ALLOW_CHANGE_PASSWD')) define('ALLOW_CHANGE_PASSWD', true);

if(!defined('ALLOW_EXEC_PLUGIN')) define('ALLOW_EXEC_PLUGIN', true);

// Auloading of classes
function __autoload($className)
{
	if (!class_exists($className, false))
	{
		if (file_exists(INCLUDE_LIB . 'classes/'.$className.'.class.php'))
			require_once(INCLUDE_LIB . 'classes/'.$className.'.class.php');
		else
			return;
	}
}

spl_autoload_register('__autoload');

$localtheme = SQLiteFactory::getTheme();
require_once(BASE_DIR . "/theme/".$localtheme."/define.php");

if(!defined('NAV_SEP')) define('NAV_SEP', 	'&nbsp;-&nbsp;');

if(!defined('NAV_NBLINK')) define('NAV_NBLINK', 10);

if(!defined('DEMO_MODE')) define('DEMO_MODE', false);

if(!defined('ADVANCED_EDITOR')) define('ADVANCED_EDITOR', true);
if(ADVANCED_EDITOR && !defined('SPAW_PATH')) {
	define('SPAW_PATH', BASE_DIR . '/spaw/');
}

if(!defined("SPAW_TOOLBAR_STYLE")) define("SPAW_TOOLBAR_STYLE", "sqlitemanager");

if(DEBUG) {
	error_reporting(E_ALL);
} else {
	error_reporting(E_ALL ^ E_NOTICE);
}

if(!defined('LEFT_FRAME_WIDTH')) 		define('LEFT_FRAME_WIDTH', 200);
if(!defined('TEXTAREA_NB_COLS')) 		define('TEXTAREA_NB_COLS', 65);
if(!defined('TEXAREA_NB_ROWS'))			define('TEXAREA_NB_ROWS', 5);
if(!defined('PARTIAL_TEXT_SIZE'))		define('PARTIAL_TEXT_SIZE', 20);
if(!defined('DISPLAY_EMPTY_ITEM_LEFT'))	define('DISPLAY_EMPTY_ITEM_LEFT', true);
if(!defined('BROWSE_NB_RECORD_PAGE'))	define('BROWSE_NB_RECORD_PAGE', 20);
if(!defined('ALLOW_FULLSEARCH'))		define('ALLOW_FULLSEARCH', true);
if(!defined('JSCALENDAR_USE'))			define('JSCALENDAR_USE', true);
if(!defined('JSCALENDAR_PATH'))			define('JSCALENDAR_PATH', 'jscalendar/');

?>
