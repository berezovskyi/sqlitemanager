<?php
/**
* Web based SQLite management
* Show and manage 'FUNCTION' properties
* @package SQLiteManager
* @author Frédéric HENNINOT
* @version $Id: functproperties.php,v 1.6 2004/11/25 12:56:18 tpruvot Exp $ $Revision: 1.6 $
*/

$functProp = new SQLiteFunctionProperties($workDb);
switch($action){
	case '':
	default:
		$functProp->PropView();
		break;
	case 'modify':
	case 'add':
		$functProp->functEditForm();
		break;
	case 'save':
	case 'delete':
		$functProp->saveProp();
		break;
	case 'export':
		$export = new SQLiteExport($workDb);
		break;
}

?>

</body>
</html>
