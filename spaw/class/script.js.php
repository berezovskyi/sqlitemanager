  // control registration array
  var spaw_editors = new Array();
  
  // returns true if editor is already registered
  function SPAW_editor_registered(editor)
  {
    var found = false;
    for(var i=0;i<spaw_editors.length;i++)
    {
      if (spaw_editors[i] == editor)
      {
        found = true;
        break;
      }
    }
    return(found);
  }
  
  // onsubmit
  function SPAW_UpdateFields()
  {
    for (var i=0; i<spaw_editors.length; i++)
    {
      SPAW_updateField(spaw_editors[i], null);
    }
  }
  
  // adds event handler for the form to update hidden fields
  function SPAW_addOnSubmitHandler(editor)
  {
    thefield = SPAW_getFieldByEditor(editor, null);

    var sTemp = "";
    oForm = document.all[thefield].form;
    if(oForm.onsubmit != null) {
      sTemp = oForm.onsubmit.toString();
      iStart = sTemp.indexOf("{") + 2;
      sTemp = sTemp.substr(iStart,sTemp.length-iStart-2);
    }
    if (sTemp.indexOf("SPAW_UpdateFields();") == -1)
    {
      oForm.onsubmit = new Function("SPAW_UpdateFields();" + sTemp);
    }
  }

  // editor initialization
  function SPAW_editorInit(editor, css_stylesheet, direction)
  {
     
     if (this[editor+'_rEdit'].document.designMode != 'on')
     {
      this[editor+'_rEdit'].document.designMode = 'On';
     }
 
     // check if the editor completely loaded and schedule to try again if not
     if (this[editor+'_rEdit'].document.readyState != 'complete')
     {
       setTimeout(function(){SPAW_editorInit(editor, css_stylesheet, direction);},20);
       return;
     }

     // register the editor 
  // prevent from executing twice on the same editor
     if (!SPAW_editor_registered(editor))
     {
       spaw_editors[spaw_editors.length] = editor;
     }
  
     // add on submit handler
     SPAW_addOnSubmitHandler(editor);
 
     if (this[editor+'_rEdit'].document.readyState == 'complete')
     {
       this[editor+'_rEdit'].document.createStyleSheet(css_stylesheet);
       this[editor+'_rEdit'].document.body.dir = direction;
       this[editor+'_rEdit'].document.body.innerHTML = document.all[editor].value;
SPAW_toggle_borders(editor,this[editor+'_rEdit'].document.body,null);
 
       // hookup active toolbar related events
       this[editor+'_rEdit'].document.onkeyup = function() { SPAW_onkeyup(editor); }
       this[editor+'_rEdit'].document.onmouseup = function() { SPAW_update_toolbar(editor, true); }
       
       // initialize toolbar
       spaw_context_html = "";
       SPAW_update_toolbar(editor, true);
     }
  }  
  
  
  function SPAW_showColorPicker(editor,curcolor) {
    return showModalDialog('<?php echo $GLOBALS["spaw_dir"]?>dialogs/colorpicker.php?lang=' + document.all['SPAW_'+editor+'_lang'].value + '&theme=' + document.all['SPAW_'+editor+'_theme'].value, curcolor, 
      'dialogHeight:250px; dialogWidth:366px; resizable:no; status:no');  
  }

  function SPAW_bold_click(editor, sender)
  {
    window.frames[editor+'_rEdit'].focus();     
    this[editor+'_rEdit'].document.execCommand('bold', false, null);
    SPAW_update_toolbar(editor, true);    
  }

  function SPAW_italic_click(editor, sender)
  {
    window.frames[editor+'_rEdit'].focus();     
   	this[editor+'_rEdit'].document.execCommand('italic', false, null);
    SPAW_update_toolbar(editor, true);    
  }

  function SPAW_underline_click(editor, sender)
  {
    window.frames[editor+'_rEdit'].focus();     
    this[editor+'_rEdit'].document.execCommand('underline', false, null);
    SPAW_update_toolbar(editor, true);    
  }
  
  function SPAW_left_click(editor, sender)
  {
    window.frames[editor+'_rEdit'].focus();     
    this[editor+'_rEdit'].document.execCommand('justifyleft', false, null);
    SPAW_update_toolbar(editor, true);    
  }

  function SPAW_center_click(editor, sender)
  {
    window.frames[editor+'_rEdit'].focus();     
  	this[editor+'_rEdit'].document.execCommand('justifycenter', false, null);
    SPAW_update_toolbar(editor, true);    
  }

  function SPAW_right_click(editor, sender)
  {
    window.frames[editor+'_rEdit'].focus();     
  	this[editor+'_rEdit'].document.execCommand('justifyright', false, null);
    SPAW_update_toolbar(editor, true);    
  }

  function SPAW_justify_click(editor, sender)
  {
    window.frames[editor+'_rEdit'].focus();     
  	this[editor+'_rEdit'].document.execCommand('justifyfull', false, null);
    SPAW_update_toolbar(editor, true);    
  }
  
  function SPAW_ordered_list_click(editor, sender)
  {
    window.frames[editor+'_rEdit'].focus();     
  	this[editor+'_rEdit'].document.execCommand('insertorderedlist', false, null);
    SPAW_update_toolbar(editor, true);    
  }

  function SPAW_bulleted_list_click(editor, sender)
  {
    window.frames[editor+'_rEdit'].focus();     
  	this[editor+'_rEdit'].document.execCommand('insertunorderedlist', false, null);
    SPAW_update_toolbar(editor, true);    
  }
  
  function SPAW_fore_color_click(editor, sender)
  {
    window.frames[editor+'_rEdit'].focus();     

    var fCol = SPAW_showColorPicker(editor,null);

    if(fCol != null)
      this[editor+'_rEdit'].document.execCommand('forecolor', false, fCol);

    SPAW_update_toolbar(editor, true);    
  }

  function SPAW_bg_color_click(editor, sender)
  {
    window.frames[editor+'_rEdit'].focus();     

    var bCol = SPAW_showColorPicker(editor,null);
    
    if(bCol != null)
    	this[editor+'_rEdit'].document.execCommand('backcolor', false, bCol);

    SPAW_update_toolbar(editor, true);    
  }

  /*
  // using standard IE link dialog
  function SPAW_hyperlink_click(editor, sender)
  {
    window.frames[editor+'_rEdit'].focus();     
  	var l = this[editor+'_rEdit'].document.execCommand('createlink');
    SPAW_update_toolbar(editor, true);    
  }
  */
  
  function SPAW_getA(editor)
  {
  	var aControl = window.frames[editor+'_rEdit'].document.selection.createRange();
    if (window.frames[editor+'_rEdit'].document.selection.type != "Control")
    {
    	aControl = aControl.parentElement();
    }
    else
    {
    	aControl = aControl(0);
    }
    
	while ((aControl.tagName != 'A') && (aControl.tagName != 'BODY'))
    {
      aControl = aControl.parentElement;
    }
    if (aControl.tagName == 'A')
      return(aControl);
    else
      return(null);
  }

  function SPAW_hyperlink_click(editor, sender)
  {
    window.frames[editor+'_rEdit'].focus();     

  	var a = SPAW_getA(editor);
	
    var aProps = {};
	// get anchors on the page
	aProps.anchors = new Array();
	var links = this[editor+'_rEdit'].document.getElementsByTagName('A');
	var aln = 0;
	if (links != null) aln = links.length;
	for (var i=0;i<aln;i++)
	{
		if (links[i].name != null && links[i].name != '')
			aProps.anchors[aProps.anchors.length] = links[i].name;
	}

	if (a)
	{
	  // edit hyperlink
      aProps.href = SPAW_stripAbsoluteUrl(editor, a.href);
      aProps.name = a.name;
      aProps.target = a.target;
      aProps.title = a.title;
  
      var naProps = showModalDialog('<?php echo $GLOBALS["spaw_dir"]?>dialogs/a.php?lang=' + document.all['SPAW_'+editor+'_lang'].value + '&theme=' + document.all['SPAW_'+editor+'_theme'].value, aProps, 
        'dialogHeight:220px; dialogWidth:366px; resizable:no; status:no');  
      
      if (naProps)  
      {
	  	if (!naProps.href && !naProps.name)
		{
			// remove hyperlink
			a.outerHTML = a.innerHTML;
		}
		else
		{
			// set link properties
		  	if (naProps.href)
		        a.href = naProps.href;
			else
				a.removeAttribute('href',0);
			if (naProps.name)
		        a.name = naProps.name;
			else
				a.removeAttribute('name',0);
			if (naProps.target && naProps.target!='_self')
		        a.target = naProps.target;
			else
				a.removeAttribute('target',0);
			if (naProps.title)
		        a.title = naProps.title;
			else
				a.removeAttribute('title',0);
	
		  	a.removeAttribute('onclick',0);
		}
      }      
	}
	else
	{
	  // new hyperlink
      var naProps = showModalDialog('<?php echo $GLOBALS["spaw_dir"]?>dialogs/a.php?lang=' + document.all['SPAW_'+editor+'_lang'].value + '&theme=' + document.all['SPAW_'+editor+'_theme'].value, aProps, 
        'dialogHeight:220px; dialogWidth:366px; resizable:no; status:no');  
      
      if (naProps)  
      {
		var a;
		if (naProps.name)
		{
		  a = document.createElement('<A NAME="'+naProps.name+'"></A>');
		}
		else
		{
		  a = document.createElement('A');
		}
	  	if (naProps.href)
	        a.href = naProps.href;
		if (naProps.target && naProps.target!='_self')
	        a.target = naProps.target;
		if (naProps.title)
	        a.title = naProps.title;
		if (window.frames[editor+'_rEdit'].document.selection.type == "Control")
		{
	        var selection = window.frames[editor+'_rEdit'].document.selection.createRange();
			a.innerHTML = selection(0).outerHTML;
			selection(0).outerHTML = a.outerHTML;
		}
		else
		{
	        var selection = window.frames[editor+'_rEdit'].document.selection.createRange();
		    if (selection.htmlText == '')
			  a.innerHTML = (a.href && a.href!='')?a.href:a.name;
		    else
			  a.innerHTML = selection.htmlText;
		
		    selection.pasteHTML(a.outerHTML);      
		}
      }      
	}

    SPAW_update_toolbar(editor, true);    
  }

  function SPAW_internal_link_click(editor, sender)
  {
    window.frames[editor+'_rEdit'].focus();     

  	var a = SPAW_getA(editor);
	
	if (a)
	{
	  // edit hyperlink
      var aProps = {};
      aProps.href = SPAW_stripAbsoluteUrl(editor, a.href);
      aProps.name = a.name;
      aProps.target = a.target;
      aProps.title = a.title;
	  aProps.description = a.innerHTML;
  
      var naProps = showModalDialog('<?php echo $spaw_internal_link_script?>?lang=' + document.all['SPAW_'+editor+'_lang'].value + '&theme=' + document.all['SPAW_'+editor+'_theme'].value, aProps, 
        'dialogHeight:220px; dialogWidth:366px; resizable:yes; status:no');  
      
      if (naProps)  
      {
	  	if (naProps.href)
	        a.href = naProps.href;
		else
			a.removeAttribute('href',0);
		if (naProps.name)
	        a.name = naProps.name;
		else
			a.removeAttribute('name',0);
		if (naProps.target && naProps.target!='_self')
	        a.target = naProps.target;
		else
			a.removeAttribute('target',0);
		if (naProps.title)
	        a.title = naProps.title;
		else
			a.removeAttribute('title',0);
		if (naProps.description)
			a.innerHTML = naProps.description;
      }      
	}
	else
	{
	  // new hyperlink
      var naProps = showModalDialog('<?php echo $spaw_internal_link_script?>?lang=' + document.all['SPAW_'+editor+'_lang'].value + '&theme=' + document.all['SPAW_'+editor+'_theme'].value, null, 
        'dialogHeight:220px; dialogWidth:366px; resizable:no; status:no');  
      
      if (naProps)  
      {
		var a;
		if (naProps.name)
		{
		  a = document.createElement('<A NAME="'+naProps.name+'"></A>');
		}
		else
		{
		  a = document.createElement('A');
		}
	  	if (naProps.href)
	        a.href = naProps.href;
		if (naProps.target && naProps.target!='_self')
	        a.target = naProps.target;
		if (naProps.title)
	        a.title = naProps.title;

		if (window.frames[editor+'_rEdit'].document.selection.type == "Control")
		{
	        var selection = window.frames[editor+'_rEdit'].document.selection.createRange();
			a.innerHTML = selection(0).outerHTML;
			selection(0).outerHTML = a.outerHTML;
		}
		else
		{
	        var selection = window.frames[editor+'_rEdit'].document.selection.createRange();
			if (naProps.description)
			{
				a.innerHTML = naProps.description;
			}
			else
			{
			    if (selection.htmlText == '')
					a.innerHTML = (a.href)?a.href:a.name;
			    else
				    a.innerHTML = selection.htmlText;
			}	
		    selection.pasteHTML(a.outerHTML);      
		}
      }      
	}

    SPAW_update_toolbar(editor, true);    
  }
  
  function SPAW_image_insert_click(editor, sender)
  {
    window.frames[editor+'_rEdit'].focus();     

    var imgSrc = showModalDialog('<?php echo $GLOBALS["spaw_dir"]?>dialogs/img_library.php?lang=' + document.all['SPAW_'+editor+'_lang'].value + '&theme=' + document.all['SPAW_'+editor+'_theme'].value+'&request_uri='+escape(window.location.href), '', 
      'dialogHeight:420px; dialogWidth:420px; resizable:no; status:no');
    
    if(imgSrc != null)    
    	this[editor+'_rEdit'].document.execCommand('insertimage', false, imgSrc);

    SPAW_update_toolbar(editor, true);    
  }
  
  function SPAW_image_prop_click(editor, sender)
  {
    var im = SPAW_getImg(editor); // current cell
    
    if (im)
    {
      var iProps = {};
      iProps.src = SPAW_stripAbsoluteUrlFromImg(editor, im.src);
      iProps.alt = im.alt;
      iProps.width = (im.style.width)?im.style.width:im.width;
      iProps.height = (im.style.height)?im.style.height:im.height;
      iProps.border = im.border;
      iProps.align = im.align;
      iProps.hspace = im.hspace;
      iProps.vspace = im.vspace;
  
      var niProps = showModalDialog('<?php echo $GLOBALS["spaw_dir"]?>dialogs/img.php?lang=' + document.all['SPAW_'+editor+'_lang'].value + '&theme=' + document.all['SPAW_'+editor+'_theme'].value, iProps, 
        'dialogHeight:200px; dialogWidth:366px; resizable:no; status:no');  
      
      if (niProps)  
      {
        im.src = (niProps.src)?niProps.src:'';
        if (niProps.alt) {
          im.alt = niProps.alt;
        }
        else
        {
          im.removeAttribute("alt",0);
        }
        im.align = (niProps.align)?niProps.align:'';
        im.width = (niProps.width)?niProps.width:'';
        //im.style.width = (niProps.width)?niProps.width:'';
        im.height = (niProps.height)?niProps.height:'';
        //im.style.height = (niProps.height)?niProps.height:'';
        if (niProps.border) {
          im.border = niProps.border;
        }
        else
        {
          im.removeAttribute("border",0);
        }
        if (niProps.hspace) {
          im.hspace = niProps.hspace;
        }
        else
        {
          im.removeAttribute("hspace",0);
        }
        if (niProps.vspace) {
          im.vspace = niProps.vspace;
        }
        else
        {
          im.removeAttribute("vspace",0);
        }
      }      
      //SPAW_updateField(editor,"");
    } // if im
    SPAW_update_toolbar(editor, true);    
  }

  function SPAW_image_popup_click(editor, sender)
  {
    window.frames[editor+'_rEdit'].focus();     
	
  	var a = SPAW_getA(editor);

    var imgSrc = showModalDialog('<?php echo $GLOBALS["spaw_dir"]?>dialogs/img_library.php?lang=' + document.all['SPAW_'+editor+'_lang'].value + '&theme=' + document.all['SPAW_'+editor+'_theme'].value+'&request_uri='+escape(window.location.href), '', 
      'dialogHeight:420px; dialogWidth:420px; resizable:no; status:no');
    
    if(imgSrc != null)    
	{
		if (a)
		{
			// edit hyperlink
			a.href="#";
			a.onclick="window.open('<?php echo $spaw_img_popup_url?>?img_url="+SPAW_stripAbsoluteUrlFromImg(editor, imgSrc)+"','Image','width=500,height=300,scrollbars=no,toolbar=no,location=no,status=no,resizable=yes,screenX=120,screenY=100');return false;";
		}
		else
		{
			var a;
			a = document.createElement('A');
			a.href="#";
			a.onclick="window.open('<?php echo $spaw_img_popup_url?>?img_url="+SPAW_stripAbsoluteUrlFromImg(editor, imgSrc)+"','Image','width=500,height=300,scrollbars=no,toolbar=no,location=no,status=no,resizable=yes,screenX=120,screenY=100');return false;";


			if (window.frames[editor+'_rEdit'].document.selection.type == "Control")
			{
		        var selection = window.frames[editor+'_rEdit'].document.selection.createRange();
				a.innerHTML = selection(0).outerHTML;
				selection(0).outerHTML = a.outerHTML;
			}
			else
			{
				var selection = window.frames[editor+'_rEdit'].document.selection.createRange();
				if (selection.htmlText == '')
					a.innerHTML = (a.href)?a.href:a.name;
				else
					a.innerHTML = selection.htmlText;
		
				selection.pasteHTML(a.outerHTML);      
			}
		}	
	}	
	

    SPAW_update_toolbar(editor, true);    
  }
  
  function SPAW_hr_click(editor, sender)
  {
    window.frames[editor+'_rEdit'].focus();     
    this[editor+'_rEdit'].document.execCommand('inserthorizontalrule', false, null);
    SPAW_update_toolbar(editor, true);    
  }

  function SPAW_copy_click(editor, sender)
  {
    window.frames[editor+'_rEdit'].focus();     
    this[editor+'_rEdit'].document.execCommand('copy', false, null);
    SPAW_update_toolbar(editor, true);    
  }

  function SPAW_paste_click(editor, sender)
  {
    window.frames[editor+'_rEdit'].focus();     
    this[editor+'_rEdit'].document.execCommand('paste', false, null);
    SPAW_update_toolbar(editor, true);    
  }
  
  function SPAW_cut_click(editor, sender)
  {
    window.frames[editor+'_rEdit'].focus();     
    this[editor+'_rEdit'].document.execCommand('cut', false, null);
    SPAW_update_toolbar(editor, true);    
  }

  function SPAW_delete_click(editor, sender)
  {
    window.frames[editor+'_rEdit'].focus();     
    this[editor+'_rEdit'].document.execCommand('delete', false, null);
    SPAW_update_toolbar(editor, true);    
  }

  function SPAW_indent_click(editor, sender)
  {
    window.frames[editor+'_rEdit'].focus();     
    this[editor+'_rEdit'].document.execCommand('indent', false, null);
    SPAW_update_toolbar(editor, true);    
  }

  function SPAW_unindent_click(editor, sender)
  {
    window.frames[editor+'_rEdit'].focus();     
    this[editor+'_rEdit'].document.execCommand('outdent', false, null);
    SPAW_update_toolbar(editor, true);    
  }

  function SPAW_undo_click(editor, sender)
  {
    window.frames[editor+'_rEdit'].focus();     
    this[editor+'_rEdit'].document.execCommand('undo','',null);
    SPAW_update_toolbar(editor, true);    
  }

  function SPAW_redo_click(editor, sender)
  {
    window.frames[editor+'_rEdit'].focus();     
    this[editor+'_rEdit'].document.execCommand('redo', false, null);
    SPAW_update_toolbar(editor, true);    
  }
  
  
  function SPAW_getParentTag(editor)
  {
    var trange = this[editor+'_rEdit'].document.selection.createRange();
    if (window.frames[editor+'_rEdit'].document.selection.type != "Control")
    {
      return (trange.parentElement());
    }
    else
    {
      return (trange(0));    
    }
  }

  // trim functions  
  function SPAW_ltrim(txt)
  {
    var spacers = " \t\r\n";
    while (spacers.indexOf(txt.charAt(0)) != -1)
    {
      txt = txt.substr(1);
    }
    return(txt);
  }
  function SPAW_rtrim(txt)
  {
    var spacers = " \t\r\n";
    while (spacers.indexOf(txt.charAt(txt.length-1)) != -1)
    {
      txt = txt.substr(0,txt.length-1);
    }
    return(txt);
  }
  function SPAW_trim(txt)
  {
    return(SPAW_ltrim(SPAW_rtrim(txt)));
  }


  
  // is selected text a full tags inner html?
  function SPAW_isFoolTag(editor, el)
  {
    var trange = this[editor+'_rEdit'].document.selection.createRange();
    var ttext;
    if (trange != null) ttext = SPAW_trim(trange.htmlText);
    if (ttext != SPAW_trim(el.innerHtml))
      return false;
    else
      return true;
  }
  
  function SPAW_style_change(editor, sender)
  {
    classname = sender.options[sender.selectedIndex].value;
    
    window.frames[editor+'_rEdit'].focus();     

    var el = SPAW_getParentTag(editor);
    if (el != null && el.tagName.toLowerCase() != 'body')
    {
      if (classname != 'default')
        el.className = classname;
      else
        el.removeAttribute('className',0);
    }
    else if (el.tagName.toLowerCase() == 'body')
    {
      if (classname != 'default')
        this[editor+'_rEdit'].document.body.innerHTML = '<p class="'+classname+'">'+this[editor+'_rEdit'].document.body.innerHTML+'</p>';
      else
        this[editor+'_rEdit'].document.body.innerHTML = '<p>'+this[editor+'_rEdit'].document.body.innerHTML+'</p>';
    }
    sender.selectedIndex = 0;

    SPAW_update_toolbar(editor, true);    
  }

  function SPAW_font_change(editor, sender)
  {
    fontname = sender.options[sender.selectedIndex].value;
    
    window.frames[editor+'_rEdit'].focus();
    
    if (fontname == null || fontname == '')
    {
      this[editor+'_rEdit'].document.execCommand('RemoveFormat', false, null);
    }
    else   
    {
      this[editor+'_rEdit'].document.execCommand('fontname', false, fontname);
    }

    sender.selectedIndex = 0;

    SPAW_update_toolbar(editor, true);    
  }

  function SPAW_fontsize_change(editor, sender)
  {
    fontsize = sender.options[sender.selectedIndex].value;
    
    window.frames[editor+'_rEdit'].focus();     

    this[editor+'_rEdit'].document.execCommand('fontsize', false, fontsize);

    sender.selectedIndex = 0;

    SPAW_update_toolbar(editor, true);    
  }

  function SPAW_paragraph_change(editor, sender)
  {
    format = sender.options[sender.selectedIndex].value;
    
    window.frames[editor+'_rEdit'].focus();     

    this[editor+'_rEdit'].document.execCommand('formatBlock', false, format);

    sender.selectedIndex = 0;

    SPAW_update_toolbar(editor, true);    
  }
    
  function SPAW_table_create_click(editor, sender)
  {
    if (window.frames[editor+'_rEdit'].document.selection.type != "Control")
    {
      // selection is not a control => insert table 
      var nt = showModalDialog('<?php echo $GLOBALS["spaw_dir"]?>dialogs/table.php?lang=' + document.all['SPAW_'+editor+'_lang'].value + '&theme=' + document.all['SPAW_'+editor+'_theme'].value+'&request_uri='+escape(window.location.href), null, 
        'dialogHeight:250px; dialogWidth:366px; resizable:no; status:no');  
       
      if (nt)
      {
        window.frames[editor+'_rEdit'].focus();     
    
        var newtable = document.createElement('TABLE');
        try {
		  if (nt.width)
          	newtable.width = nt.width;
		  if (nt.height)
          	newtable.height = nt.height;
		  if (nt.border)
          	newtable.border = nt.border;
          if (nt.cellPadding) 
		  	newtable.cellPadding = nt.cellPadding;
          if (nt.cellSpacing) 
		  	newtable.cellSpacing = nt.cellSpacing;
		  if (nt.bgColor)
          	newtable.bgColor = nt.bgColor;
		  if (nt.background)
		  	newtable.background = nt.background;
		  if (nt.className)
		  	newtable.className = nt.className;
          
          // create rows
          for (var i=0;i<parseInt(nt.rows);i++)
          {
            var newrow = document.createElement('TR');
            for (var j=0; j<parseInt(nt.cols); j++)
            {
              var newcell = document.createElement('TD');
              newrow.appendChild(newcell);
            }
            newtable.appendChild(newrow);
          }
          var selection = window.frames[editor+'_rEdit'].document.selection.createRange();
        	selection.pasteHTML(newtable.outerHTML);      
          SPAW_toggle_borders(editor, window.frames[editor+'_rEdit'].document.body, null);
          SPAW_update_toolbar(editor, true);    
        }
        catch (excp)
        {
          alert('error');
        }
      }
    }
  }
  
  function SPAW_table_prop_click(editor, sender)
  {
    window.frames[editor+'_rEdit'].focus();     

    var tTable
    // check if table selected
    if (window.frames[editor+'_rEdit'].document.selection.type == "Control")
    { 
      var tControl = window.frames[editor+'_rEdit'].document.selection.createRange();
      if (tControl(0).tagName == 'TABLE')
      {
        tTable = tControl(0);
      }
    }
    else
    {
      var tControl = window.frames[editor+'_rEdit'].document.selection.createRange();
      tControl = tControl.parentElement();
      while ((tControl.tagName != 'TABLE') && (tControl.tagName != 'BODY'))
      {
        tControl = tControl.parentElement;
      }
      if (tControl.tagName == 'TABLE')
        tTable = tControl;
      else
        return false;
    }

    var tProps = {};
    tProps.width = (tTable.style.width)?tTable.style.width:tTable.width;
    tProps.height = (tTable.style.height)?tTable.style.height:tTable.height;
    tProps.border = tTable.border;
    tProps.cellPadding = tTable.cellPadding;
    tProps.cellSpacing = tTable.cellSpacing;
    tProps.bgColor = tTable.bgColor;
	tProps.className = tTable.className;
	tProps.background = tTable.background;

    var ntProps = showModalDialog('<?php echo $GLOBALS["spaw_dir"]?>dialogs/table.php?lang=' + document.all['SPAW_'+editor+'_lang'].value + '&theme=' + document.all['SPAW_'+editor+'_theme'].value+'&request_uri='+escape(window.location.href), tProps, 
      'dialogHeight:250px; dialogWidth:366px; resizable:no; status:no');  
    
    if (ntProps)
    {
      // set new settings
	  if (ntProps.width)
	    tTable.width = ntProps.width;
	  else
	  	tTable.removeAttribute('width',0);
   	  tTable.style.width = (ntProps.width)?ntProps.width:'';
      if (ntProps.height)
	  	tTable.height = ntProps.height
	  else
	  	tTable.removeAttribute('height',0);
      tTable.style.height = (ntProps.height)?ntProps.height:'';
	  if (ntProps.border)
	  	tTable.border = ntProps.border;
	  else
	  	tTable.removeAttribute('border',0);
      if (ntProps.cellPadding) 
	  	tTable.cellPadding = ntProps.cellPadding;
	  else
	  	tTable.removeAttribute('cellpadding',0);
      if (ntProps.cellSpacing) 
	  	tTable.cellSpacing = ntProps.cellSpacing;
	  else
	  	tTable.removeAttribute('cellspacing',0);
	  if (ntProps.bgColor)
      	tTable.bgColor = ntProps.bgColor;
	  else
	  	tTable.removeAttribute('bgcolor',0);
	  if (ntProps.background)
	  	tTable.background = ntProps.background;
	  else
	  	tTable.removeAttribute('background',0);
	  if (ntProps.className)
	  	tTable.className = ntProps.className;
	  else
	  	tTable.removeAttribute('className',0);

      SPAW_toggle_borders(editor, tTable, null);
    }

    SPAW_update_toolbar(editor, true);    
    //SPAW_updateField(editor,"");
  }
  
  // edits table cell properties
  function SPAW_table_cell_prop_click(editor, sender)
  {
    var cd = SPAW_getTD(editor); // current cell
    
    if (cd)
    {
      var cProps = {};
      cProps.width = (cd.style.width)?cd.style.width:cd.width;
      cProps.height = (cd.style.height)?cd.style.height:cd.height;
      cProps.bgColor = cd.bgColor;
	  cProps.background = cd.background;
      cProps.align = cd.align;
      cProps.vAlign = cd.vAlign;
      cProps.className = cd.className;
      cProps.noWrap = cd.noWrap;
      cProps.styleOptions = new Array();
      if (document.all['SPAW_'+editor+'_tb_style'] != null)
      {
        cProps.styleOptions = document.all['SPAW_'+editor+'_tb_style'].options;
      }
  
      var ncProps = showModalDialog('<?php echo $GLOBALS["spaw_dir"]?>dialogs/td.php?lang=' + document.all['SPAW_'+editor+'_lang'].value + '&theme=' + document.all['SPAW_'+editor+'_theme'].value+'&request_uri='+escape(window.location.href), cProps, 
        'dialogHeight:220px; dialogWidth:366px; resizable:no; status:no');  
      
      if (ncProps)  
      {
	  	if (ncProps.align)
			cd.align = ncProps.align;
		else
			cd.removeAttribute('align',0);
		if (ncProps.vAlign)
        	cd.vAlign = ncProps.vAlign;
		else
			cd.removeAttribute('valign',0);
		if (ncProps.width)
        	cd.width = ncProps.width;
		else
			cd.removeAttribute('width',0);
        cd.style.width = (ncProps.width)?ncProps.width:'';
		if (ncProps.height)
        	cd.height = ncProps.height;
		else
			cd.removeAttribute('height',0);
        cd.style.height = (ncProps.height)?ncProps.height:'';
		if (ncProps.bgColor)
        	cd.bgColor = ncProps.bgColor;
		else
			cd.removeAttribute('bgcolor',0);
	    if (ncProps.background)
		  	cd.background = ncProps.background;
		else
		  	cd.removeAttribute('background',0);
		if (ncProps.className)
        	cd.className = ncProps.className;
		else
			cd.removeAttribute('className',0);
		if (ncProps.noWrap)
        	cd.noWrap = ncProps.noWrap;
		else
			cd.removeAttribute('nowrap',0);
      }      
    }
    SPAW_update_toolbar(editor, true);    
    //SPAW_updateField(editor,"");
  }

  // returns current table cell  
  function SPAW_getTD(editor)
  {
    if (window.frames[editor+'_rEdit'].document.selection.type != "Control")
    {
      var tControl = window.frames[editor+'_rEdit'].document.selection.createRange();
      tControl = tControl.parentElement();
      while ((tControl.tagName != 'TD') && (tControl.tagName != 'TH') && (tControl.tagName != 'TABLE') && (tControl.tagName != 'BODY'))
      {
        tControl = tControl.parentElement;
      }
      if ((tControl.tagName == 'TD') || (tControl.tagName == 'TH'))
        return(tControl);
      else
        return(null);
    }
    else
    {
      return(null);
    }
  }

  // returns current table row  
  function SPAW_getTR(editor)
  {
    if (window.frames[editor+'_rEdit'].document.selection.type != "Control")
    {
      var tControl = window.frames[editor+'_rEdit'].document.selection.createRange();
      tControl = tControl.parentElement();
      while ((tControl.tagName != 'TR') && (tControl.tagName != 'TABLE') && (tControl.tagName != 'BODY'))
      {
        tControl = tControl.parentElement;
      }
      if (tControl.tagName == 'TR')
        return(tControl);
      else
        return(null);
    }
    else
    {
      return(null);
    }
  }
  
  // returns current table  
  function SPAW_getTable(editor)
  {
    if (window.frames[editor+'_rEdit'].document.selection.type == "Control")
    { 
      var tControl = window.frames[editor+'_rEdit'].document.selection.createRange();
      if (tControl(0).tagName == 'TABLE')
        return(tControl(0));
      else
        return(null);
    }
    else
    {
      var tControl = window.frames[editor+'_rEdit'].document.selection.createRange();
      tControl = tControl.parentElement();
      while ((tControl.tagName != 'TABLE') && (tControl.tagName != 'BODY'))
      {
        tControl = tControl.parentElement;
      }
      if (tControl.tagName == 'TABLE')
        return(tControl);
      else
        return(null);
    }
  }
  
  // returns selected image
  function SPAW_getImg(editor) {
    if (window.frames[editor+'_rEdit'].document.selection.type == "Control")
    { 
      var tControl = window.frames[editor+'_rEdit'].document.selection.createRange();
      if (tControl(0).tagName == 'IMG')
        return(tControl(0));
      else
        return(null);
    }
    else
    {
      return(null);
    }
  }

  function SPAW_table_row_insert_click(editor, sender)
  {
    var ct = SPAW_getTable(editor); // current table
    var cr = SPAW_getTR(editor); // current row

    if (ct && cr)
    {
      var newr = ct.insertRow(cr.rowIndex+1);
      for (var i=0; i<cr.cells.length; i++)
      {
        if (cr.cells(i).rowSpan > 1)
        {
          // increase rowspan
          cr.cells(i).rowSpan++;
        }
        else
        {
          var newc = cr.cells(i).cloneNode();
          newr.appendChild(newc);
        }
      }
      // increase rowspan for cells that were spanning through current row
      for (var i=0; i<cr.rowIndex; i++)
      {
        var tempr = ct.rows(i);
        for (var j=0; j<tempr.cells.length; j++)
        {
          if (tempr.cells(j).rowSpan > (cr.rowIndex - i))
            tempr.cells(j).rowSpan++;
        }
      }
    }
    SPAW_update_toolbar(editor, true);    
  } // insertRow
  
  function SPAW_formCellMatrix(ct)
  {
    var tm = new Array();
    for (var i=0; i<ct.rows.length; i++)
      tm[i]=new Array();

    for (var i=0; i<ct.rows.length; i++)
    {
      jr=0;
      for (var j=0; j<ct.rows(i).cells.length;j++)
      {
        while (tm[i][jr] != undefined) 
          jr++;

        for (var jh=jr; jh<jr+(ct.rows(i).cells(j).colSpan?ct.rows(i).cells(j).colSpan:1);jh++)
        {
          for (var jv=i; jv<i+(ct.rows(i).cells(j).rowSpan?ct.rows(i).cells(j).rowSpan:1);jv++)
          {
            if (jv==i)
            {
              t